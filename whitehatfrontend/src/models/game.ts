export interface Game {
  id: string;

  launchCode: string;

  name: string;

  gameProviderId: number;

  rtp: number;
}
