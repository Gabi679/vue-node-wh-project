
export interface DetailedGame{
  "id": number,
  "launchCode": string,
  "rtp": string,
  "provider": string,
  "hot": number,
  "new": number
}
