export interface Category{
  id: string;
  name: string;
  category: string;
}
