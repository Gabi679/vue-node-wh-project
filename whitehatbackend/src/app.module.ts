import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MysqlDBConfigService } from './database/mysql-db-config.service';
import { GameModule } from './game/game.module';

@Module({
  imports: [
    GameModule,
    TypeOrmModule.forRootAsync({
      useClass: MysqlDBConfigService,
      inject: [MysqlDBConfigService],
    }),
  ],
  controllers: [],
  providers: [MysqlDBConfigService],
})
export class AppModule {}
