import { Injectable } from '@nestjs/common';
import { TypeOrmModuleOptions, TypeOrmOptionsFactory } from '@nestjs/typeorm';
import * as process from 'process';
import { Game } from '../game/entities/game.model';
import { Country } from '../game/entities/country.model';
import { Brand } from '../game/entities/brand.model';
import { GameProviders } from '../game/entities/game-provider.model';
import { GameCountryBlock } from '../game/entities/game-country-block.model';
import { BrandGames } from '../game/entities/brand-games.model';
import { GameBrandBlock } from '../game/entities/game-brand-block.model';
import {Category} from "../game/entities/category.entity";

@Injectable()
export class MysqlDBConfigService implements TypeOrmOptionsFactory {
  constructor() {}

  createTypeOrmOptions(): TypeOrmModuleOptions {
    return {
      type: 'mysql',
      host: process.env.MYSQL_HOST,
      username: process.env.MYSQL_USER,
      password: process.env.MYSQL_PASSWORD,
      database: process.env.MYSQL_DATABASE,
      entities: [
        Game,
        GameProviders,
        GameCountryBlock,
        GameBrandBlock,
        BrandGames,
        Country,
        Brand,
        Category
      ],
    };
  }
}
