import { Controller, Get, Query } from '@nestjs/common';
import { CategoryService } from '../services/category.service';
import { Category } from '../entities/category.entity';

@Controller('category')
export class CategoryController {
  constructor(private readonly categoryService: CategoryService) {}

  @Get('getByBrandId')
  async getByBrandId(@Query('brandId') brandId: number): Promise<Category[]> {
    return this.categoryService.getByBrandId(brandId);
  }
}
