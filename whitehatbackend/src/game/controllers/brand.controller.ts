import { Controller, Get } from '@nestjs/common';
import { Brand } from '../entities/brand.model';
import { BrandService } from '../services/brand.service';

@Controller('brand')
export class BrandController {
  constructor(private readonly brandService: BrandService) {}

  @Get('all')
  async getBrands(): Promise<Brand[]> {
    return this.brandService.getBrands();
  }
}
