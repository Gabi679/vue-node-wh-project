import { Controller, Get, Query } from '@nestjs/common';
import { GameService } from '../services/game.service';
import { Game } from '../entities/game.model';
import {DetailedGameDto} from "../dto/detailed-game.dto";

@Controller('games')
export class GameController {
  constructor(private readonly gameService: GameService) {}

  @Get('gamesByCountryAndBrand')
  async gamesByCountryAndBrand(
    @Query('brand') brandId: string,
    @Query('countryCode') countryCode: string,
    @Query('category') category: string | undefined,
  ): Promise<Game[]> {
    return this.gameService.gamesByCountryAndBrand(
      brandId,
      countryCode,
      category,
    );
  }
  @Get('getGameDetails')
  async getGameDetails(
    @Query('brand') brandId: string,
    @Query('launchCode') launchCode: string
  ): Promise<DetailedGameDto> {
    return this.gameService.getGameDetails(
        launchCode,
        brandId
    );
  }

  @Get('all')
  async getGames(): Promise<Game[]> {
    return this.gameService.getGames();
  }
}
