import { Controller, Get } from '@nestjs/common';
import { CountryService } from '../services/country.service';
import { Country } from '../entities/country.model';

@Controller('country')
export class CountryController {
  constructor(private readonly countryService: CountryService) {}

  @Get('all')
  async getCountries(): Promise<Country[]> {
    return this.countryService.getCountries();
  }
}
