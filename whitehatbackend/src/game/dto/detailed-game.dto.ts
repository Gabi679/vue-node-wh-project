
export class DetailedGameDto{
    id: string;

    launchCode: string;

    name: string;

    provider: string;

    rtp: number;

    hot: string;

    new: string;
}