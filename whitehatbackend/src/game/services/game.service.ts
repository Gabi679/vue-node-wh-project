import { Injectable } from '@nestjs/common';
import { Game } from '../entities/game.model';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { BrandGames } from '../entities/brand-games.model';
import { GameCountryBlock } from '../entities/game-country-block.model';
import { GameBrandBlock } from '../entities/game-brand-block.model';
import { DetailedGameDto } from '../dto/detailed-game.dto';
import { GameProviders } from '../entities/game-provider.model';

@Injectable()
export class GameService {
  constructor(
    @InjectRepository(Game)
    private gamesRepository: Repository<Game>,
  ) {}
  async getGames(): Promise<Game[]> {
    return this.gamesRepository.find();
  }

  async getGameDetails(
    launchCode: string,
    brandId: string,
  ): Promise<DetailedGameDto> {
    return this.gamesRepository
      .createQueryBuilder('game')
      .select([
        'game.id as id',
        'game.launchcode as launchCode',
        'game.rtp as rtp',
        'gameProvider.name as provider',
        'brandGame.hot as hot',
        'brandGame.new as new',
      ])
      .where('game.launchcode = :launchCode', { launchCode })
      .andWhere('brandGame.brandid = :brandId', { brandId })
      .leftJoin(
        BrandGames,
        'brandGame',
        'game.launchcode = brandGame.launchcode',
      )
      .leftJoin(
        GameProviders,
        'gameProvider',
        'game.provider = gameProvider.distributor',
      )
      .groupBy('game.id')
      .getRawOne<DetailedGameDto>();
  }

  async gamesByCountryAndBrand(
    brandId: string,
    countryCode: string,
    category: string | undefined,
  ): Promise<Game[]> {
    const subQueryForBlockedBrands = this.getQueryForBlockedBrands(brandId);
    const subQueryForBlockedBrandsAndCountries =
      this.getQueryForBlockedBrandsAndQueries(brandId, countryCode);
    if (category !== undefined) {
      const subQueryForBrandsInCategory =
        this.getSubQueryForBrandsInCategory(category);
      return this.gamesRepository
        .createQueryBuilder('games')
        .select()
        .where(`games.launchCode NOT IN (${subQueryForBlockedBrands})`)
        .andWhere(
          `games.launchCode NOT IN (${subQueryForBlockedBrandsAndCountries})`,
        )
        .andWhere(`games.launchCode IN (${subQueryForBrandsInCategory}) `)
        .getMany();
    } else {
      return this.gamesRepository
        .createQueryBuilder('games')
        .select()
        .where(`games.launchCode NOT IN (${subQueryForBlockedBrands})`)
        .andWhere(
          `games.launchCode NOT IN (${subQueryForBlockedBrandsAndCountries})`,
        )
        .getMany();
    }
  }

  private getSubQueryForBrandsInCategory(category: string) {
    return this.gamesRepository
      .createQueryBuilder('games')
      .subQuery()
      .select('brandGame.launchCode', 'launchCode')
      .where(`brandGame.category = '${category}'`)
      .from(BrandGames, 'brandGame')
      .getQuery();
  }

  private getQueryForBlockedBrandsAndQueries(
    brandId: string,
    countryCode: string,
  ) {
    return this.gamesRepository
      .createQueryBuilder('games')
      .subQuery()
      .select('gameCountryBlock.launchCode', 'launchCode')
      .where(`gameCountryBlock.brandid = ${brandId}`)
      .andWhere(` gameCountryBlock.country = '${countryCode}'`)
      .from(GameCountryBlock, 'gameCountryBlock')
      .getQuery();
  }

  private getQueryForBlockedBrands(brandId: string) {
    return this.gamesRepository
      .createQueryBuilder('games')
      .subQuery()
      .select('gameBrandBlock.launchCode', 'launchCode')
      .where(`gameBrandBlock.brandid = ${brandId}`)
      .from(GameBrandBlock, 'gameBrandBlock')
      .getQuery();
  }
}
