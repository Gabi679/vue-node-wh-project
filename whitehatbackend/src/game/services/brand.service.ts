import { Injectable } from '@nestjs/common';
import { Game } from '../entities/game.model';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import {Brand} from "../entities/brand.model";

@Injectable()
export class BrandService {
    constructor(
        @InjectRepository(Brand)
        private brandRepository: Repository<Brand>,
    ) {}
    async getBrands(): Promise<Brand[]> {
        return this.brandRepository.find();
    }
}
