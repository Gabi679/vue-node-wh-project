import { Injectable } from '@nestjs/common';
import { Game } from '../entities/game.model';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Category } from '../entities/category.entity';

@Injectable()
export class CategoryService {
  constructor(
    @InjectRepository(Category)
    private categoryRepository: Repository<Category>,
  ) {}
  async getByBrandId(brandId: number): Promise<Category[]> {
    return this.categoryRepository.findBy({
      brandId
    });
  }
}
