import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Game } from './entities/game.model';
import { GameProviders } from './entities/game-provider.model';
import { GameCountryBlock } from './entities/game-country-block.model';
import { BrandGames } from './entities/brand-games.model';
import { GameService } from './services/game.service';
import { GameController } from './controllers/game.controller';
import { Country } from './entities/country.model';
import { Brand } from './entities/brand.model';
import { BrandController } from './controllers/brand.controller';
import { CountryController } from './controllers/country.controller';
import { BrandService } from './services/brand.service';
import { CountryService } from './services/country.service';
import { GameBrandBlock } from './entities/game-brand-block.model';
import { Category } from './entities/category.entity';
import { CategoryController } from './controllers/category.controller';
import { CategoryService } from './services/category.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Game,
      GameProviders,
      GameCountryBlock,
      GameBrandBlock,
      BrandGames,
      Country,
      Brand,
      Category,
    ]),
  ],
  controllers: [
    GameController,
    BrandController,
    CountryController,
    CategoryController,
  ],
  providers: [GameService, BrandService, CountryService, CategoryService],
  exports: [TypeOrmModule],
})
export class GameModule {}
