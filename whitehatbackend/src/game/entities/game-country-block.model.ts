import {Entity, Column, PrimaryColumn} from 'typeorm';

@Entity('game_country_block')
export class GameCountryBlock {

  @Column()
  @PrimaryColumn()
  id: number;

  @Column({ name: 'launchcode' })
  launchCode: string;

  @Column({ name: 'brandid' })
  brandId: string;

  @Column()
  country: string;
}
