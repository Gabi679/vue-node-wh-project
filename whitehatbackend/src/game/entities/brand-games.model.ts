import {Entity, Column, PrimaryColumn} from 'typeorm';

@Entity('brand_games')
export class BrandGames {

  @Column()
  @PrimaryColumn()
  id: number;

  @Column({ name: 'launchcode' })
  launchCode: string;

  @Column({ name: 'brandid' })
  brandId: string;

  @Column()
  category: string;

  @Column()
  rtp: number;
}
