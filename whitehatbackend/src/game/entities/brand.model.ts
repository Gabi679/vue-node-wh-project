import {Entity, Column, PrimaryColumn} from 'typeorm';

@Entity('brands')
export class Brand {

    @Column()
    @PrimaryColumn()
    id: number;

    @Column()
    brand: string;
}
