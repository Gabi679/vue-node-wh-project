import {Entity, Column, PrimaryColumn} from 'typeorm';

@Entity('game_brand_block')
export class GameBrandBlock {

  @Column()
  @PrimaryColumn()
  id: number;

  @Column({ name: 'launchcode' })
  launchCode: string;

  @Column({ name: 'brandid' })
  brandId: string;
}
