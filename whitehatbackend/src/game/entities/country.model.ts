import {Entity, Column, PrimaryColumn} from 'typeorm';

@Entity('countries')
export class Country {

    @Column()
    @PrimaryColumn()
    id: number;

    @Column()
    code: string;

    @Column()
    country: string;
}
