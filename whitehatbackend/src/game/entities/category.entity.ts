import {Column, Entity, PrimaryColumn} from "typeorm";

@Entity('category')
export class Category {

    @Column()
    @PrimaryColumn()
    id: number;

    @Column()
    name: string;

    @Column({name: 'brandid'})
    brandId: number;

    @Column()
    category: number;
}
