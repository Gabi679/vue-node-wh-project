import { Entity, Column, PrimaryColumn } from 'typeorm';

@Entity('game_providers')
export class GameProviders {
  @Column()
  @PrimaryColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  distributor: string;
}
