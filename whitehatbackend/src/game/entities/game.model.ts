import {Entity, Column, PrimaryColumn} from 'typeorm';

@Entity()
export class Game {
  @Column()
  @PrimaryColumn()
  id: string;

  @Column({ name: 'launchcode' })
  launchCode: string;

  @Column()
  name: string;

  @Column({ name: 'game_provider_id' })
  gameProviderId: number;

  @Column()
  rtp: number;
}
